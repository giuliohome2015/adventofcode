with open('input_day09.txt') as f:
	lines = [int(line.strip()) for line in f.readlines()]

preamble = lines[:25]
print(preamble)
print(len(preamble))

def isvalid(num, prevlist):
  endlist = len(prevlist)
  for x in range(0,endlist):
    for y in range(x+1,endlist):
       if prevlist[x] + prevlist[y] == num:
         return True
  return False

for i in range(25,len(lines)):
  valid = isvalid(lines[i],lines[i-25:i])
  if not valid: 
    found = lines[i]
    print ('@line',i+1,'num:',lines[i])
    print('len preamble',len(lines[i-25:i]),'preamble',lines[i-25:i])
    break


def summable(num, checklist):
  curr_sum = 0
  endlist = len(checklist)
  for x in range(0,endlist):
    curr_sum += checklist[x]
    if num == curr_sum: 
      thelist = checklist[0:x+1]
      print('found answer 1',num,'sum from line',x+1,'thelist',thelist)
      return True, min(thelist),max(thelist)
    if num < curr_sum: return False,None,None
  return False,None,None

for i in range(0,len(lines)):
  valid, themin, themax = summable(found,lines[i:])
  if valid:
    print('@line',i+1,'min',themin,'max',themax,'answer 2:', themin+themax )
    break






