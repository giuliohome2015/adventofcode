with open('input_day.2020.14.txt') as f:
	lines = [l.strip().split(' = ') for l in f.readlines() if l.strip()]

check = set(op[0:4] for op, data in lines)
print(check)

# 
mem = {}  # noooo [0 for i in range(0, 2**36)]
for op, data in lines:
	if op == 'mask':
		mask = data
	elif op[0:4] == 'mem[':
		addr = int(op[4:-1])
		num = int(data)
		bnum = '{0:036b}'.format(num)
		arr = [bnum[i] if mask[i] == 'X' else mask[i] for i in range(0,36)]
		mem[addr] = int(''.join(arr),2)
	else:
		print ('error op',op)
		break 

print('answer1', sum(mem.values()))
