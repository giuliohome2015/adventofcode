
with open('input_day.2020.12.txt') as f:
	lines = [l.strip() for l in f.readlines() if len(l.strip())]

shiplat, shiplon = 0, 0
waylat, waylon = 1, 10

#face = 0 # est - 1 norht - 2 west - 3 south


def rotate_waypoint(face, waylat, waylon, shiplat, shiplon):
		if face == 0:
			return waylat, waylon	
		if face == 2:
			return -waylat, -waylon
		if face == 1:
			return waylon, -waylat	
		if face == 3:
			return -waylon, waylat

proceed = ''
for line in lines:
	move, num = line[0:1], int(line[1:])
	if move == 'N': # or (move == 'F' and face == 1):
		waylat += num
	elif move == 'S': #  or (move == 'F' and face == 3):
		waylat += -num
	elif move == 'E': # or (move == 'F' and face == 0):
		waylon += num
	elif move == 'W': # or (move == 'F' and face == 2):
		waylon += -num	
	elif move == 'L':
		face = (num / 90) % 4 # (face + (num / 90)) % 4
		waylat, waylon = rotate_waypoint(face, waylat, waylon, shiplat, shiplon)
	elif move == 'R':
		face = ((-num) / 90) % 4 # (face - (num / 90)) % 4
		waylat, waylon = rotate_waypoint(face, waylat, waylon, shiplat, shiplon)
	elif move == 'F':	
		shiplat += num * waylat
		shiplon += num * waylon
	print (move, num, shiplat, shiplon, waylat,waylon)
	if proceed != 'yes':	
		proceed = input('go?')
print('answer2',abs(shiplat)+abs(shiplon))

	
