with open('input_day.2020.14.txt') as f:
	lines = [l.strip().split(' = ') for l in f.readlines() if l.strip()]

check = set(op[0:4] for op, data in lines)
print(check)
go=''
# 
mem = {}  # noooo [0 for i in range(0, 2**36)]
for op, data in lines:
	if go != 'go': 
		go = input('go?')
	debug = go != 'go'
	if op == 'mask':
		mask = data
		if debug: print(op,data)
	elif op[0:4] == 'mem[':
		addr = int(op[4:-1])
		num = int(data)
		baddr = '{0:036b}'.format(addr)
		arr = [baddr[i] if mask[i] == '0' else mask[i] \
			for i in range(0,36)]
		options = [i for i in range(0,36) \
			if arr[i] == 'X']
		for i in range(0, 2**len(options)):
			copied = arr[:]
			current = ('{0:0' + str(len(options))  +'b}').format(i)
			for j in range(0, len(options)):
				copied[options[j]] = current[j]
			decoded = ''.join(copied)
			mem[int(decoded,2)] = num
			if debug: print(op, data, decoded)
	else:
		print ('error op',op)
		break 
answer1 = sum(mem.values())
print('answer1', answer1)
if input('copy to clipboard?') == 'yes':
	import os
	retcopy = os.system('echo -n ' + str(answer1) + ' | xclip -i -selection clipboard')
	print('copied ret code', retcopy)
	
