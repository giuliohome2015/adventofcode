with open('input_day.2020.13.txt') as f:
	txt = f.readlines()


start = int(txt[0].strip())

line2 = txt[1].strip().split(',')

buses = list(filter(lambda x: x[0],zip([int(x) if x != 'x' else None for x in line2], range(0,len(line2)))))

print(start)
print(buses)

k = buses[0][0]
z = k

sortbuses = buses[1:]
#sortbuses.sort()

for tryme, skipme in sortbuses:
	print('k',k,'tryme', tryme,'reminder',skipme)
	found = False
	for i in range(0,100000):	
		z += k 
		x  = (z + skipme) % tryme
		if x == 0:
			print('z',z,'tryme', tryme,'reminder',skipme)
			k = k * tryme
			found = True
			break	
	if not found: 
		print ('error')
		break
