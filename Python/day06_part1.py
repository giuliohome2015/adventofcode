with open('input_day06.txt') as myfile:
	txt = myfile.read().split('\n\n')

mysum=0

for curr_group in txt:
	curr_set = set()
	for user in curr_group.split('\n'):
		curr_set = curr_set.union(set(user))
	mysum += len(curr_set)

print('answer part1: ',mysum)
