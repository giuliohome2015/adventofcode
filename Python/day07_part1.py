def parsecomp(str):
	str = str.strip()
	if str.startswith('no other'):
		return None
	parts = str.split(' ')
	ret = {}
	ret['num'] = int(parts[0])
	ret['name'] = ' '.join(parts[1:-1])
	return ret

def readline(line):
	parts = line.split(' bags contain')
	ret = {}
	ret['name'] = parts[0].strip()
	parts = parts[1].strip().split(',')
	ret['tree'] = [parsecomp(part) for part in parts]
	return ret

with open('input_day07.txt') as myfile:
	trees = [readline(line) for line in myfile.readlines()]

#print(len(txt))
#print(txt[0])
#print(txt[-1])
#print(readline(txt[0]))
#print(readline(txt[-1]))

def buildtree(mylist,excluded):
	ret = []
	for tree in trees:
		for comp in tree['tree']:
			#print(comp)
			if comp and \
			  comp['name'] in mylist and \
			  tree['name'] not in excluded:
				ret.append(tree['name'])
	return ret


def builder(mylist):
	ret = []
	iter = buildtree(mylist, mylist)
	while(len(iter)):
		ret = list(set(ret + iter))
		#print('ret',ret)
		iter = buildtree(iter, ret)
	return(ret)

print('answer 1:',len(builder(['shiny gold'])))

