with open('input_day06.txt') as myfile:
	txt = myfile.read()

if txt[-1] == '\n':
	txt = txt[:-1]

txt = txt.split('\n\n')


test = '''abc

a
b
c

ab
ac

a
a
a
a

b'''.split('\n\n')

mysum=0

for curr_group in txt:
	users = curr_group.split('\n')
	curr_set = set(users[0])
	for user in users[1:]:
		curr_set = curr_set.intersection(set(user))
	# print(curr_group)
	# print(len(curr_set))
	mysum += len(curr_set)

print('answer part2: ',mysum)
