with open('input_day10.txt') as f:
	lines =[int(l.strip()) for l in f.readlines()]
lines = list(set(lines))
lines.sort()

diff1 = 0 # 1 if lines[0] == 1 else 0
diff3 = 0 # 1 if lines[0] == 3 else 0
lines = [0] + lines + [lines[len(lines)-1] + 3]
for i in range(0,len(lines) - 1):
	diff = lines[i+1] - lines[i]
	if diff < 0: print('error<0 @line: ',(i+1))
	elif diff >3: print('error>3 @line: ',(i+1))
	elif diff == 1: diff1 += 1
	elif diff == 3: diff3 += 1
#diff3 += 1 # my adapter
print('answer 1: ', diff1*diff3)

def skippable(lines):
	if len(lines) == 2: return 1
	i = 1
	while i < len(lines) - 2:
		k = 0	
		while  i + k <= len(lines) - 1 and lines[i+1+k] - lines[i-1] <= 3: 
			k += 1 	
		if k>0:		
			sum = 0
			for j in range(0,k+1):
				sum += skippable(lines[(k + i):])
			return sum
		i += 1		
	return 1
print('answer 2: ',skippable(lines))
