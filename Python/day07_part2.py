def parsecomp(str):
	str = str.strip()
	if str.startswith('no other'):
		return None
	parts = str.split(' ')
	ret = {}
	ret['num'] = int(parts[0])
	ret['name'] = ' '.join(parts[1:-1])
	return ret

def readline(line):
	parts = line.split(' bags contain')
	ret = {}
	ret['name'] = parts[0].strip()
	parts = parts[1].strip().split(',')
	ret['tree'] = [parsecomp(part) for part in parts]
	return ret

with open('input_day07.txt') as myfile:
	trees = [readline(line) for line in myfile.readlines()]

#print(len(txt))
#print(txt[0])
#print(txt[-1])
#print(readline(txt[0]))
#print(readline(txt[-1]))

def buildtree(parent):
	for tree in trees:
		
		if tree['name'] == parent:
		
			return [ comp for comp in tree['tree'] if comp]
	return []


def builder(parent):
	ret = 0
	comps = buildtree(parent)
	for comp in comps:
		#print(comp) 
		ret = ret + comp['num']*(1 + builder(comp['name'])) 
	return ret

#print(trees[141]['name'], trees[141]['name'] == 'shiny gold')
print('answer 2:',builder('shiny gold'))

