with open('input_day.2020.13.txt') as f:
	txt = f.readlines()


start = int(txt[0].strip())

buses = [int(x) for x in txt[1].strip().split(',') if x != 'x']

print(start)
print(buses)

def nextstop(start, bus):
	nextbus = bus 
	while nextbus <= start:
		nextbus += bus
	return nextbus

arrivals = [(nextstop(start,bus),bus) for bus in buses]

print(arrivals)	
solution = min(arrivals)
print(solution)
stoptime, stopbus = solution
print('answer1', (stoptime - start) * stopbus)
