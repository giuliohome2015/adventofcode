import sys
import urllib3


with open('cookie.secret', "r") as f:
    cookie = f.read()

headers = {
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36',
        "cookie": cookie # important!
    }
http = urllib3.PoolManager()
resp = http.request('GET', 
    'https://adventofcode.com/' + sys.argv[1] + '/day/' + sys.argv[2] + '/input',
    headers=headers)
print(resp.status)
input = resp.data.decode('utf-8')
with open(sys.argv[3], "w") as f:
    f.write(input)