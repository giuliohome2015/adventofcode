with open('input_day08.txt') as input:
	lines = input.readlines()

#print(len(lines), lines[0],lines[-1])



def read(line):
	parts = line.strip().split(' ')
	return parts[0], int(parts[1])


def runpgm(lines):	 	
	acc = 0
	next = 0
	visited = []
	switches = []
	while next not in visited and next<len(lines):
		visited.append(next)
		op, num =  read(lines[next])
		#print(op, num)
		if op == 'acc':
			next, acc = next+1, acc+num
			continue
		elif op == 'jmp':
			switches.append((next,'jmp',num))
			next += num
			continue
		elif op == 'nop':
			switches.append((next,'nop',num))
			next += 1
			continue
		else:
			print('error:', next, op, num)
			break
		  	
	print("loop", next in visited)
	print("next", next)
	print("acc", acc)
	return next not in visited, acc, switches

found, acc, switches = runpgm(lines)
for (switch, op, num) in switches:
	changed = list(lines)
	if op == 'nop':
		changed[switch] = changed[switch].replace(op, 'jmp')
	if op == 'jmp':
		changed[switch] = changed[switch].replace(op, 'nop')	
	found, acc, _switches = runpgm(changed)
	print('found ', found, acc)
	if found:
		break;



