
curl --cookie cookie.txt https://adventofcode.com/$1/day/$2/input >input_day.$1.$2.txt

wget --user-agent Mozilla/4.0 --load-cookies cookie.txt https://adventofcode.com/$1/day/$2/input -O wget_input_day.$1.$2.txt
wget --user-agent Mozilla/4.0 --load-cookies cookie.txt https://adventofcode.com/$1/day/$2 -O wget_html_day.$1.$2.html

echo $1 --- $2 input downloaded

cmp input_day.$1.$2.txt wget_input_day.$1.$2.txt

echo curl and get results compared
